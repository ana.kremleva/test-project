<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');
?>
<div class="overlay" id="task_add" data-task-id="">
    <?php
    $status = [1=>'В очереди', 'В работе', 'Завершина'];
    if (isset($taskId)){
        $task = propertyEl('pr', 2, $taskId);
        $people = $task[0]['PROPERTIES']['PLAYER']['VALUE']; // массив исполнителей
        $idStat = $task[0]['PROPERTIES']['STATUS']['VALUE_ENUM_ID']; // id статуса
    }
    ?>
    <div class="popup">
        <form method="post" id="ajax_form" action="" >


            <table class="table_task_add">
                <tr>
                    <td>Название</td>
                    <td  class="info">
                        <input hidden type="text" name="id" value="<?= $task['ID'] ?? ''?>">
                        <input id="name-task" type="text" name="name" value="<?= $task['NAME'] ?? ''?>">
                    </td>
                </tr>
                <tr>
                    <td>Исполнитель</td>
                    <td class="info people">
                        <select size="3" multiple name="people[]">
                            <?php
                            $peopleTask = propertyEl('el', 1);
                            foreach ($peopleTask as $key => $value){?>
                                <option <?= ((isset($people)) && (in_array($value['ID'], $people)))?'selected ' : '';?> value="<?= $value['ID']?>"><?= $value['NAME']?></option>
                            <?}?>
                        </select>
                    </td>
                </tr>
                <tr class="status_task">
                    <td>Статус</td>
                    <td>
                        <select size="1" name="status">
                            <?php foreach ($status as $key => $stat): ?>
                                <option <?= ((isset($idStat)) && ($idStat == $key)) ? 'selected' : ''; ?>
                                        value="<?= $key?>"><?= $stat ?></option>
                            <? endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="td_bottom_del">
                        <input class="bottom_del" type="submit" value="Удалить" name="but_del">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td >
                        <input class="<?= (isset($taskId))? 'bottom_change': 'bottom_save' ?>"  type="submit" value="Сохранить" id="but_save">
                    </td>
                </tr>

            </table>

        </form>
        <a class="close" title="Закрыть"></a>
        <? var_dump($_POST); ?>
        <div id="result_form">11</div>
    </div>

</div>


