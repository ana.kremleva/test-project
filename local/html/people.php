<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" href="style.css">
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="http://code.jquery.com/jquery-2.0.2.min.js"></script>
    <script src="js.js"></script>
    <title>Задачи</title>

</head>
<body>
<main class="work">
    <ul class="top-menu">
        <li><a href="task.php">Задачи</a></li>
        <li><a href="people.php">Исполнители</a></li>
    </ul>

    <table class="table_task">
        <tr>
            <th>
                id
            </th>
            <th>
                Имя
            </th>
            <th>
                Должность
            </th>
            <th>
                Действия
            </th>

        </tr>
        <tr>
            <td>1</td>
            <td>Александр</td>
            <td>Junior</td>
            <td><a href="#" class="change">редактировать</a>/<a href="#" id="1" data-val="Александр" class="delet">удалить</a>
            </td>
        </tr>
        <tr>
            <td>2</td>
            <td>Александр</td>
            <td>Junior</td>
            <td><a href="#" class="change">редактировать</a>/<a href="#" id="2" data-val="Александр" class="delet">удалить</a>
            </td>
        </tr>
        <tr>
            <td>3</td>
            <td>Александр</td>
            <td>Junior</td>
            <td><a href="#" class="change">редактировать</a>/<a href="#" id="3" data-val="Александр" class="delet">удалить</a>
            </td>
        </tr>
        <tr>
            <td>4</td>
            <td>Александр</td>
            <td>Junior</td>
            <td><a href="#" class="change">редактировать</a>/<a href="#" id="4" data-val="Александр" class="delet">удалить</a>
            </td>
        </tr>


    </table>
    <input class="bottom_task" type="submit" value="Добавить" name="but_task">


    <a class="overlay" id="task_add"></a>
    <div class="popup">
        <form>
            <table class="table_task_add">
                <tr>
                    <td>Имя</td>
                    <td  class="info">
                        <input type="text" name="name_task">
                    </td>
                </tr>
                <tr>
                    <td>Должность</td>
                    <td class="info people">
                        <input type="text" name="name_task">
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td class="td_bottom_del">
                        <input class="bottom_del" type="submit" value="Удалить" name="but_del">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td >
                        <input class="bottom_save" type="submit" value="Сохранить" name="but_save">
                    </td>
                </tr>

            </table>
        </form>

        <a class="close" title="Закрыть"></a>
    </div>



</main>


</body>
</html>