<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" href="style.css">
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="http://code.jquery.com/jquery-2.0.2.min.js"></script>
    <script src="js.js"></script>
    <title>Задачи</title>

</head>
<body>
<main class="work">
    <ul class="top-menu">
        <li><a href="task.php">Задачи</a></li>
        <li><a href="people.php">Исполнители</a></li>
    </ul>

    <table class="table_task">
        <tr>
            <th>
                id
            </th>
            <th>
                Название
            </th>
            <th>
                Исполнитель
            </th>
            <th>
                Статус
            </th>
            <th>
                Действия
            </th>
        </tr>
        <tr>
            <td>1</td>
            <td>Решить задачу</td>
            <td>Александр</td>
            <td>В очереди</td>
            <td><a href="#" class="change">редактировать</a>/<a href="#" id="1" data-val="Решить задачу" class="delet">удалить</a>
            </td>
        </tr>
        <tr>
            <td>2</td>
            <td>Решить задачу</td>
            <td>Александр</td>
            <td>В работе</td>
            <td><a href="#" class="change">редактировать</a>/<a href="#" id="2" data-val="Решить задачу" class="delet">удалить</a>
            </td>
        </tr>
        <tr>
            <td>3</td>
            <td>Решить задачу</td>
            <td>Александр</td>
            <td>Завершина</td>
            <td><a href="#" class="change">редактировать</a>/<a href="#" id="3" data-val="Решить задачу" class="delet">удалить</a>
            </td>
        </tr>
        <tr>
            <td>3</td>
            <td>Решить задачу</td>
            <td>Александр</td>
            <td>Завершина</td>
            <td><a href="#" class="change">редактировать</a>/<a href="#" id="3" data-val="Решить задачу" class="delet">удалить</a>
            </td>
        </tr>
        <tr>
            <td>3</td>
            <td>Решить задачу</td>
            <td>Александр</td>
            <td>Завершина</td>
            <td><a href="#" class="change">редактировать</a>/<a href="#" id="3" data-val="Решить задачу" class="delet">удалить</a>
            </td>
        </tr>
        <tr>
            <td>3</td>
            <td>Решить задачу</td>
            <td>Александр</td>
            <td>Завершина</td>
            <td><a href="#" class="change">редактировать</a>/<a href="#" id="3" data-val="Решить задачу" class="delet">удалить</a>
            </td>
        </tr>

    </table>
    <input class="bottom_task" type="submit" value="Добавить" name="but_task">


    <a class="overlay" id="task_add"></a>
    <div class="popup">
        <form>
            <table class="table_task_add">
                <tr>
                    <td>Название</td>
                    <td  class="info">
                        <input type="text" name="name_task">
                    </td>
                </tr>
                <tr>
                    <td>Исполнитель</td>
                    <td class="info people">
                        <select size="1" name="people">
                            <option selected value="Чебурашка">Чебурашка</option>
                            <option value="Крокодил Гена">Крокодил Гена</option>
                            <option value="Шапокляк">Шапокляк</option>
                            <option value="Крыса Лариса">Крыса Лариса</option>
                        </select>
                    </td>
                </tr>
                <tr class="status_task">
                    <td>Статус</td>
                    <td>
                        <select size="1" name="status">
                            <option selected value="В очереди">В очереди</option>
                            <option value="В работе">В работе</option>
                            <option value="Завершина">Завершина</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="td_bottom_del">
                        <input class="bottom_del" type="submit" value="Удалить" name="but_del">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td >
                        <input class="bottom_save" type="submit" value="Сохранить" name="but_save">
                    </td>
                </tr>

            </table>
        </form>

        <a class="close" title="Закрыть"></a>
    </div>



</main>


</body>
</html>