<?php
if (!function_exists('pre')) {
    function pre($var, $die = false)
    {
        echo '<pre>';
        print_r($var);
        echo '</pre>';
        if ($die)
            die('Debug in PRE');
    }
}

if (!function_exists('vd')) {
    function vd($var, $die = false)
    {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
        if ($die)
            die('Debug in VD');
    }
}


/**
 * Функция бирикс
 * @param $vibor параметр выбора,
 * 'el' - выберет все элементы инфоблока(по умолчанию),
 * 'pr' - определенный элемент ИБ с параметрами
 * @param $iblock_id  id инфоблока
 * @param string $element_id id элемента, если не указывать выведи последний элемент инфоблока
 * @return array|void возвращает массив данных элелемента или массив элементов
 * @throws \Bitrix\Main\LoaderException
 */
function propertyEl ($vibor = 'el', $iblock_id, $element_id = '') {
    if (\Bitrix\Main\Loader::includeModule('iblock'))
    {
        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
        $arFilter = Array("IBLOCK_ID"=>$iblock_id, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
        switch ($vibor){
            case 'el':
                while($ob = $res->GetNextElement()){
                    $arFields[] = $ob->GetFields();
                }
                break;
            case 'pr':
                while($ob = $res->GetNextElement()){
                    $arFields = $ob->GetFields();
                    $arFields[]['PROPERTIES'] = $ob->GetProperties();
                    if ($arFields['ID'] == $element_id) break;
                }

                break;
        }

        return $arFields;
    }
};



/**
 * Функция для транслитерации
 * @param $value строка для замены
 * @return string
 */

function translit($value)
{
    $converter = array(
        'а' => 'a',    'б' => 'b',    'в' => 'v',    'г' => 'g',    'д' => 'd',
        'е' => 'e',    'ё' => 'e',    'ж' => 'zh',   'з' => 'z',    'и' => 'i',
        'й' => 'y',    'к' => 'k',    'л' => 'l',    'м' => 'm',    'н' => 'n',
        'о' => 'o',    'п' => 'p',    'р' => 'r',    'с' => 's',    'т' => 't',
        'у' => 'u',    'ф' => 'f',    'х' => 'h',    'ц' => 'c',    'ч' => 'ch',
        'ш' => 'sh',   'щ' => 'sch',  'ь' => '',     'ы' => 'y',    'ъ' => '',
        'э' => 'e',    'ю' => 'yu',   'я' => 'ya',

        'А' => 'A',    'Б' => 'B',    'В' => 'V',    'Г' => 'G',    'Д' => 'D',
        'Е' => 'E',    'Ё' => 'E',    'Ж' => 'Zh',   'З' => 'Z',    'И' => 'I',
        'Й' => 'Y',    'К' => 'K',    'Л' => 'L',    'М' => 'M',    'Н' => 'N',
        'О' => 'O',    'П' => 'P',    'Р' => 'R',    'С' => 'S',    'Т' => 'T',
        'У' => 'U',    'Ф' => 'F',    'Х' => 'H',    'Ц' => 'C',    'Ч' => 'Ch',
        'Ш' => 'Sh',   'Щ' => 'Sch',  'Ь' => '',     'Ы' => 'Y',    'Ъ' => '',
        'Э' => 'E',    'Ю' => 'Yu',   'Я' => 'Ya',
    );

    $value = strtr($value, $converter);
    return $value;
}