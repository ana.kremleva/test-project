<?php
use Bitrix\Main\Page\Asset;
?>
<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php $APPLICATION->ShowTitle(); ?></title>
    <?php $APPLICATION->ShowHead(); ?>
    <?php
    //var_dump(SITE_TEMPLATE_PATH . '/css/style.css');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.css"); ?>

    <title>Задачи</title>

</head>
<body>
<div id="panel">
    <? $APPLICATION->ShowPanel(); ?> <!-- Отображение панели администратора -->
</div>
<main class="work">

    <?php
    $APPLICATION->IncludeComponent("bitrix:menu", "top_menu", array(
        "ROOT_MENU_TYPE" => "top",    // Тип меню для первого уровня
        "MAX_LEVEL" => "1",    // Уровень вложенности меню
        "CHILD_MENU_TYPE" => "top",    // Тип меню для остальных уровней
        "USE_EXT" => "Y",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
        "DELAY" => "N",    // Откладывать выполнение шаблона меню
        "ALLOW_MULTI_SELECT" => "Y",    // Разрешить несколько активных пунктов одновременно
        "MENU_CACHE_TYPE" => "N",    // Тип кеширования
        "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
        "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
        "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
    ),
        false
    ); ?>
