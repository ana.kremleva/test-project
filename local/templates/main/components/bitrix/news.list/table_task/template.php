<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<form method="post" id="task_change" action="">
    <table class="table_task">
        <tr>
            <th>
                id
            </th>
            <th>
                Название
            </th>
            <th>
                Исполнитель
            </th>
            <th>
                Статус
            </th>
            <th>
                Действия
            </th>
        </tr>
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <tr>
                <td><?= $arItem['ID'] ?></td>
                <td><?= $arItem['NAME'] ?></td>
                <td>
                    <?php
                    //массив исполнителей
                    $arPlayer = $arItem['PROPERTIES']['PLAYER']['VALUE'];
                    //id привязанного ИБ
                    $linkIBlok = $arItem['PROPERTIES']['PLAYER']['LINK_IBLOCK_ID'] * 1;
                    if (count($arPlayer) > 1) {
                        foreach ($arPlayer as $val) {
                            $player = propertyEl('pr', $linkIBlok, $val);
                            echo $player['NAME'] . '<br>';
                        }
                    } elseif (count($arPlayer) == 1) {
                        $player = propertyEl('pr', $linkIBlok, $arPlayer[0]);
                        echo $player['NAME'];
                    } else {
                        echo 'Исполнителя нет';
                    }


                    ?>
                </td>
                <td><?= $arItem['PROPERTIES']['STATUS']['VALUE'] ?></td>
                <td>
                    <!--<input class="change" type="submit" value="редактировать">-->
                    <a id="<?= $arItem['ID'] ?>" class="change"><input hidden name="form" value="tsk">редактировать</a>/
                    <a id="<?= $arItem['ID'] ?>" data-name="<?= $arItem['NAME'] ?>" data-form="task_change" class="delet">удалить</a>
                </td>
            </tr>
        <? endforeach; ?>
    </table>
</form>






