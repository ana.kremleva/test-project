<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<form method="post" id="people_change" action="">
    <table class="table_people">
        <tr>
            <th>
                id
            </th>
            <th>
                Имя
            </th>
            <th>
                Должность
            </th>
            <th>
                Действия
            </th>

        </tr>
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <tr>
                <td><?= $arItem['ID']; ?></td>
                <td><?= $arItem['NAME']; ?></td>
                <td><?= $arItem['PROPERTIES']['POST']['VALUE']; ?></td>
                <td>
                    <a id="<?= $arItem['ID'] ?>" class="change"><input hidden name="form" value="man">редактировать</a>/
                    <a id="<?= $arItem['ID'] ?>" data-name="<?= $arItem['NAME'] ?>" data-form="people_change" class="delet">удалить</a>
                </td>
            </tr>

        <? endforeach; ?>

    </table>

</form>

