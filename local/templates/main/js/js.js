$(document).ready(function () {

    /*выбор активного пункта меню*/
    $('.top-menu a').each(function () {
        if (this.href == window.location.href) {
            $(this.parentNode).addClass('active');
        }

    })


    /*удаление записи из таблицы на странице*/


    $('.delet').click(function () {

        var tr = $(this).closest('tr');
        var id = tr.prev().context.id;
        var name = tr.prev().context.dataset.name;
        var form = tr.prev().context.dataset.form;
        var form = form.indexOf('people');
        if (form == -1) {
            var question = (confirm("Удалить задачу '" + name + "' c id " + id + "?"));
        }
        if (form !== -1) {
            var question = (confirm("Удалить запись о сотруднике '" + name + "' c id " + id + "?"));
        }
        if (question) {
            tr.remove();
            sendAjaxForm('', '/ajax/ajaxForm.php', 'delet', id);
            return false
        }
        ;

    })


    /*вызов мод окна "добавить задачу"*/
    $('.bottom_task').click(function () {
        positionAjaxForm('task_change');
    });

    $('.bottom_people').click(function () {
        positionAjaxForm('people_change');
    });


    /*вызов мод окна "редактировать задачу"*/
    $(".table_task").on("click", ".change", (function () {
        var tr = $(this).closest('tr');
        var id = tr.prev().context.id;
        positionAjaxForm('task_change',id);
    }));

    $(".table_people").on("click", ".change", (function () {
        var tr = $(this).closest('tr');
        var id = tr.prev().context.id;
        console.log(id);
        positionAjaxForm('people_change',id);
    }));


    function positionAjaxForm(form,id) {
        $.ajax({
            url: '/ajax/formAjax.php',
            method: 'post',
            dataType: 'html',
            headers: {'id': id},
            data: $("#" + form).serialize(),
            success: function (data) {
                $('body').append(data);
            },
            error: function (response) { // Данные не отправлены
                console.log('Ошибка. Данные не отправлены.');
            }
        });
    }


    /*закрыть мод окна "добавить задачу"*/
    $(document).on('click', '.close', function () {

        $(".overlay").removeClass().addClass("overlay");

    });


    /*сохранение новой задачи*/
    $(document).on('click', '.bottom_save', function () {
        var form = $(this);
        var ajax = form.prev().context.dataset.form;

        sendAjaxForm(ajax, '/ajax/ajaxForm.php', 'save');
        return false;
    });


    /*редактирование задачи*/
    $(document).on('click', '.bottom_change', function () {

        var form = $(this);
        var ajax = form.prev().context.dataset.form;

        sendAjaxForm(ajax, '/ajax/ajaxForm.php', 'change');
        return false;
    });


    function sendAjaxForm(ajax_form, url, step, id) {
        $.ajax({
            url: url, //url страницы (action_ajax_form.php)
            type: "POST", //метод отправки
            dataType: "html", //формат данных
            headers: {
                'bot': step,
                'id': id
            },
            data: $("#" + ajax_form).serialize(),  // Сеарилизуем объект
            success: function (response) { //Данные отправлены успешно
                result = $.parseJSON(response);console.log(result);
                $('.table_people').append(result);
/*
                myFunc();
                $('#result_form').html(result);*/
            },
            error: function (response) { // Данные не отправлены
                console.log('Ошибка. Данные не отправлены.');
            }
        });
    }

    //Дополнительный совет - не загрязняйте глобальное пространство переменными, лучше объявите функцию, которая будет добавлять вашу разметку в таблицу и объявляйте переменные в ней
    function myFunc() {

        //Для начала, вам нужно найти элемент, в который нужно вставить вашу разметку. Вданном случае таблицу с id='table'.
        var table = document.getElementById('table');

        //Тут выдерните ваше значение из locslStorage, я просто присвою 1.
        var faucet1 = 1;
        //Теперь создаем строку и присваиваем ее переменной.
        var tr = document.createElement("tr");

        if (faucet1 == 1) {

            //добавляем разметку в созданную строку
            tr.innerHTML = '<td>гег </td> <td> </td> <td> гег</td> <td> гег </td> <td> гег</td>';

            //вставляем строку в таблицу
            table.appendChild(tr);

        } else {

            console.log('error');

        }

    };


})







