<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock'); ?>

<?
if (isset($_POST['form'])) {
    (empty($_SERVER['HTTP_ID'])) ?: $elId = $_SERVER['HTTP_ID'];
    if ($_POST['form'] == 'tsk') : ?>
        <div class="overlay change" id="task_add" data-task-id="">
            <?php


            $status = [1 => 'В очереди', 'В работе', 'Завершина'];
            if (isset($elId)) {
                $task = propertyEl('pr', 2, $elId);
                $people = $task[0]['PROPERTIES']['PLAYER']['VALUE']; // массив исполнителей
                $idStat = $task[0]['PROPERTIES']['STATUS']['VALUE_ENUM_ID']; // id статуса
            }
            ?>
            <div class="popup">
                <form method="post" id="ajax_form_t" action="">
                    <table class="table_task_add">
                        <tr>
                            <td>Название</td>
                            <td class="info">
                                <input hidden type="text" name="form" value="task">
                                <input hidden type="text" name="id" value="<?= $task['ID'] ?? '' ?>">
                                <input id="name-task" type="text" name="name" value="<?= $task['NAME'] ?? '' ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Исполнитель</td>
                            <td class="info people">
                                <select size="3" multiple name="people[]">
                                    <?php
                                    $peopleTask = propertyEl('el', 1);
                                    foreach ($peopleTask as $key => $value) {
                                        ?>
                                        <option <?= ((isset($people)) && (in_array($value['ID'], $people))) ? 'selected ' : ''; ?>
                                                value="<?= $value['ID'] ?>"><?= $value['NAME'] ?></option>
                                    <? } ?>
                                </select>
                            </td>
                        </tr>
                        <tr class="status_task">
                            <td>Статус</td>
                            <td>
                                <select size="1" name="status">
                                    <?php foreach ($status as $key => $stat): ?>
                                        <option <?= ((isset($idStat)) && ($idStat == $key)) ? 'selected' : ''; ?>
                                                value="<?= $key ?>"><?= $stat ?></option>
                                    <? endforeach; ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="td_bottom_del">
                                <?
                                if (isset($elId)) { ?>
                                    <input class="bottom_del" type="submit" value="Удалить" name="but_del">
                                <? } ?>

                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input class="<?= (isset($elId)) ? 'bottom_change' : 'bottom_save' ?>" type="submit"
                                       value="Сохранить" data-form="ajax_form_t" id="but_save">
                            </td>
                        </tr>

                    </table>


                </form>


                <br>


                <a class="close" title="Закрыть"></a>

            </div>
        </div>

    <? endif;
    if ($_POST['form'] == 'man') :
        if (isset($elId)) $people = propertyEl('pr', 1, $elId); ?>
        <div class="overlay change" id="people_add" data-task-id="">
            <div class="popup">
                <form method="post" id="ajax_form_p" action="">
                    <?
                    //var_dump($_SERVER["REQUEST_URI"]);
                    //var_dump($_SERVER["HTTP_HOST"]);
                    var_dump($_SERVER["HTTP_REFERER"]);

                    ?>
                    <table class="table_task_add">
                        <tr>
                            <td>Имя</td>
                            <td class="info">
                                <input hidden type="text" name="form" value="man">
                                <input hidden type="text" name="id" value="<?= $people['ID'] ?? '' ?>">
                                <input type="text" name="name" value="<?= $people['NAME'] ?? '' ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Должность</td>
                            <td class="info_people">
                                <input type="text" name="post" value="<?= $people[0]['PROPERTIES']['POST']['VALUE'] ?? '' ?>">
                            </td>
                        </tr>

                        <tr>
                            <td></td>
                            <td class="td_bottom_del">
                                <?
                                if (isset($elId)) { ?>
                                    <input class="bottom_del" type="submit" value="Удалить" name="but_del">
                                <? } ?>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input href="<?=$_SERVER["HTTP_REFERER"]?>" class="<?= (isset($elId)) ? 'bottom_change' : 'bottom_save' ?>" type="submit"
                                       value="Сохранить" data-form="ajax_form_p" id="but_save">
                            </td>
                        </tr>

                    </table>

                </form>

                <a class="close" title="Закрыть"></a>
            </div>
        </div>
    <? endif;
} ?>



